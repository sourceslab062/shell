wget -q https://github.com/LineageOS/android_prebuilts_gcc_linux-x86_aarch64_aarch64-linux-android-4.9/archive/refs/heads/lineage-19.1.zip
unzip lineage-19.1.zip -d ../
mv ../android_prebuilts_gcc_linux-x86_aarch64_aarch64-linux-android-4.9 ../gcc

wget -q https://gitlab.com/sourceslab062/aosp-clang/-/archive/r412851/aosp-clang-r412851.tar.bz2
tar xjf aosp-clang-r412851.tar.bz2 -C ../
mv ../aosp-clang-r412851 ../aosp

wget https://gitlab.com/sourceslab062/fog-kernel/-/raw/shell/script/compile.sh?ref_type=heads&inline=false

wget https://gitlab.com/sourceslab062/fog-kernel/-/raw/shell/conf/miui11-halium_defconfig?ref_type=heads&inline=false

apt install -y build-essential git bc kmod cpio flex cpio libncurses5-dev bison libssl-dev libelf-dev curl unzip python2

mkdir out 