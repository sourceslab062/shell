#defconf="mi8937_defconfig"
#defconf="ubports_olive_defconfig"
#defconf="halium_mi8937_defconfig"
defconf="vendor/miui11-halium_defconfig"
#defconf="fog-miu11_defconfig"
#defconf="vendor/fog-perf_defconfig"
#baseDir="/home/nix/linG/home/nix/kernel"
CORES=4;
baseDir="/workspace"
#defconf="lavender-perf_defconfig"
# Get compiler string
    KBUILD_COMPILER_STRING=\
$("${baseDir}"/clang/bin/clang --version | head -n 1 | \
perl -pe 's/\(http.*?\)//gs' | sed -e 's/  */ /g' -e 's/[[:space:]]*$//')
set CONFIG_BUILD_ARM64_DT_OVERLAY=y
export KBUILD_COMPILER_STRING
export PATH="${baseDir}"/clang/bin:"${baseDir}"/gcc:${PATH}
#make O=out ARCH=arm64 mrproper && 
make O=out ARCH=arm64 $defconf && make O=out ARCH=arm64 \
                SUBARCH="ARM64" \
  		CROSS_COMPILE="aarch64-linux-android-" \
  CROSS_COMPILE_ARM32="arm-linux-androideabi-" \
  CC="clang" \
  AR="llvm-ar" \
  AS="llvm-as" \
  NM="llvm-nm" \
  LD="aarch64-linux-android-ld" \
  STRIP="aarch64-linux-android-strip" \
  OBJCOPY="llvm-objcopy" \
  OBJDUMP="llvm-objdump" \
  OBJSIZE="llvm-size" \
  READELF="aarch64-linux-android-readelf" \
  HOSTCC="clang" \
  HOSTCXX="clang++" \
  HOSTAR="llvm-ar" \
  CLANG_TRIPLE="aarch64-linux-gnu-" \
  STRIP=llvm-strip -j4 2>&1 | tee kernel.log
#SUBARCH=arm64 \
	#	HOSTCC=clang mrproper \
